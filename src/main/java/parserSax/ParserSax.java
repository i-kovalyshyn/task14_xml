package parserSax;

import model.Banks;
import model.Type;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class ParserSax extends DefaultHandler {
    private Banks bank;
    public Type type;
    private List<Banks> banks = new ArrayList<>();
    private boolean bName = false;
    private boolean bCountry = false;
    private boolean bDepositor = false;
    private boolean bAccountId = false;
    private boolean bAmountOnDeposit = false;
    private boolean bProfitability = false;
    private boolean bTimeConstraints = false;
    private boolean bType = false;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("banks")) {
            bank = new Banks();
            bank.setName(attributes.getValue("name"));
        }
        switch (qName) {
            case "name":
                bName = true;
                break;
            case "country":
                bCountry = true;
                break;
            case "depositor":
                bDepositor = true;
                break;

        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (bName) {
            bank.setName(new String(ch, start, length));
            bName = false;
        }
        if (bCountry) {
            bank.setCountry(new String(ch, start, length));
            bCountry = false;
        }
        if (bDepositor) {
            bank.setDepositor(new String(ch, start, length));
            bDepositor = false;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("bank")) {
           // bank.getType(type);
            banks.add(bank);
        }
    }

    public List<Banks> getBank() {
        return banks;
    }
}
