import model.Banks;
import org.xml.sax.SAXException;
import parserSax.ParserSax;
import validator.XmlValidator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        String xml = "/home/ihor/Desktop/EPAM/task14_xml/src/main/resources/banksXML.xml";
        String xsd = "/home/ihor/Desktop/EPAM/task14_xml/src/main/resources/banksXSD.xsd";

        boolean result = XmlValidator.validateXsd(xml, xsd);
        if (result) {
            System.out.println("XML is correct");
        } else {
            System.out.println("XML is incorrect");
        }

        ParserSax parserSax = new ParserSax();
        File xmlDocument = Paths.get(xml).toFile();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(xmlDocument, parserSax);
        }catch (ParserConfigurationException | SAXException | IOException e){
            e.printStackTrace();
        }
        List<Banks> banksList = parserSax.getBank();
        banksList.forEach(System.out::println);

    }
}
