<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <div style="background-color: green; color: black;">
                    <h2>Banks INFO</h2>
                </div>
                <table border="3">
                    <tr bgcolor="2E9AFE">
                        <th>Name</th>
                        <th>Country</th>
                        <th>Depositor</th>
                        <th>amountOnDeposit</th>
                    </tr>

                    <xsl:for-each select="banks/bank">
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="country"/></td>
                            <td><xsl:value-of select="depositor"/></td>
                            <td><xsl:value-of select="amountOnDeposit"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>